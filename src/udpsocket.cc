#include "udpsocket.h"
#include <chrono>
#include <thread>
#include <cassert>
#include <cstring>
#include <unistd.h>
#include <sys/socket.h> // socket
#include <netdb.h> // getprotobyname
#include <net/if.h> // struct ifreq

namespace net {

#if defined(MSG_NOSIGNAL)
    const int UdpSocket::IO_FLAGS = MSG_NOSIGNAL;
#else
    const int UdpSocket::IO_FLAGS = 0;
#endif

    UdpSocket::UdpSocket() {
        init();
    }

    UdpSocket::~UdpSocket() {
        close();
    }

    void UdpSocket::close() {

        if (!isClosed()) {
            const auto retryDelay = std::chrono::milliseconds(CLOSE_RETRY_DELAY_MSECS);
            
            /* retry if close() fails due to delivery of a signal
             * to prevent descriptor leak
             */
            for (int count=0
                ;::close(descriptor_)==-1 && errno==EINTR && count<MAX_CLOSE_RETRIES
                ;++count) {

                std::this_thread::sleep_for(retryDelay);
            }

            descriptor_ = INVALID_FD;
        }
    }

    void UdpSocket::init() {
        assert(descriptor_ == INVALID_FD);

        // TODO: error message when UDP not available
        const auto *udp_proto = getprotobyname("udp");
        assert(udp_proto != nullptr);

        descriptor_ = socket(PF_INET,SOCK_DGRAM,udp_proto->p_proto);

        if (descriptor_ == -1) {
            // TODO: log error
            return;
        }
        
#       if defined(SO_NOSIGPIPE)
            // Block SIGPIPE, the BSD/Apple way
            setsockopt(SOL_SOCKET,SO_NOSIGPIPE,1);
#       endif // SO_NOSIGPIPE
    }

    bool UdpSocket::setsockopt(int level, int flag, int value) {
        assert(!isClosed());
        const int ivalue = value;
        return -1 != ::setsockopt(descriptor_,level,flag,&ivalue,sizeof(ivalue));
    }

    bool UdpSocket::setAllowBroadcasts(bool flag) {
        return setsockopt(SOL_SOCKET,SO_BROADCAST,flag);
    }
        
    bool UdpSocket::setReuseAddress(bool flag) {
        return setsockopt(SOL_SOCKET,SO_REUSEADDR,flag);
    }

    bool UdpSocket::attachToDevice(const std::string& device) {
        assert(!isClosed());
        struct ifreq ifr;
        std::memset(&ifr,0,sizeof(ifreq));
        std::strncpy(ifr.ifr_name,device.c_str(),sizeof(ifr.ifr_name)-1);
        return -1 != ::setsockopt(descriptor_,SOL_SOCKET,SO_BINDTODEVICE,
                                  &ifr,sizeof(ifr));
    }

    bool UdpSocket::bind(const IPv4Address& addr) {
        assert(!isClosed());
        return -1!=::bind(descriptor_,addr.ptr(),addr.size());
    }

    bool UdpSocket::sendto(const std::string& data, const IPv4Address& dest) {
        assert(!isClosed());
        return (ssize_t)data.size() == ::sendto(descriptor_,
                                       data.data(),data.size(),
                                       IO_FLAGS,
                                       dest.ptr(),dest.size());
    }
    
    bool UdpSocket::recvfrom(std::string& data, IPv4Address& src) {
        assert(!isClosed());
        // TODO: keep state
        data.resize(recv_buffer_size_);
        socklen_t addr_size = src.size();
        ssize_t result = ::recvfrom(descriptor_,&data[0],data.size(),IO_FLAGS,src.ptr(),&addr_size);
        if (result > 0) {
            data.resize(result);
            return true;
        }
        return false;
    }

} // namespace net

