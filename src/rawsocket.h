#ifndef RANDOM_RAW_SOCKET_H_INCLUDED
#define RANDOM_RAW_SOCKET_H_INCLUDED

#include "net.h"

#include <netinet/in.h> // struct sockaddr_in
#include <cinttypes>
#include <cassert>

namespace net {

    class RawSocket {
        public:
            RawSocket(const std::string& interfaceName);
            ~RawSocket();
            
            void close();

            bool isClosed() const { 
                return descriptor_ == -1;
            }

            bool setBlocking(bool wantBlocking = true) {
                return helper::setBlockingIO(descriptor_,wantBlocking);
            }

            bool setsockopt(int level, int flag, int value);

            //bool bind(const IPv4Address& address);
            
            bool sendto(const std::string& data, const MacAddress& target);
            bool recvfrom(std::string& data, MacAddress& source);

            void setRecvBufferSize(size_t size) {
                recv_buffer_size_ = size;
            }

        private:
            void init(const std::string& interfaceName);

            static constexpr int MAX_CLOSE_RETRIES = 10;
            static constexpr int CLOSE_RETRY_DELAY_MSECS = 100;
            static constexpr int INVALID_FD = -1;
            static constexpr size_t DEFAULT_RECV_BUFFER_SIZE = 1024;
            static const int IO_FLAGS; // for sendto() and recvfrom() MSG_NOSIGNAL

            int descriptor_ = INVALID_FD;
            size_t recv_buffer_size_ = DEFAULT_RECV_BUFFER_SIZE;

            MacAddress source_address_;
            int iface_idx_ = -1;
    };
}

#endif //RANDOM_RAW_SOCKET_H_INCLUDED
