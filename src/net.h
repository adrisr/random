#ifndef RANDOM_NET_H_INCLUDED
#define RANDOM_NET_H_INCLUDED

#include <string>
#include <array>
#include <netinet/in.h> // struct sockaddr_in
#include <cinttypes>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <fcntl.h> // fcntl

namespace net {
    // struct sockaddr_in wrapper
    class IPv4Address {
        public:
            static constexpr uint32_t ANY_ADDRESS = INADDR_ANY,
                                      BROADCAST_ADDRESS = INADDR_BROADCAST;
            
            IPv4Address() : IPv4Address(0,0) {}

            IPv4Address(const std::string& dotted_quad, uint16_t port) {
                init(parse_dotted_quad_address(dotted_quad),port);
            }

            IPv4Address(uint32_t ip, uint16_t port) {
                init(ip,port);
            }

            const struct sockaddr *ptr() const {
                return (struct sockaddr*) &data_;
            }

            struct sockaddr *ptr() {
                return (struct sockaddr*) &data_;
            }

            socklen_t size() const {
                return sizeof(struct sockaddr_in);
            }
            
            bool isIPv4() const {
                return data_.sin_family == AF_INET;
            }
            
            std::string str() const;
            
            unsigned short port() const {
                return htons(data_.sin_port);
            }

        private:
            static uint32_t parse_dotted_quad_address(const std::string& s);

            void init(uint32_t ip, uint16_t port);

            struct sockaddr_in data_;
    };

    // mac address wrapper
    struct MacAddress {
        public:
            static constexpr size_t LENGTH = ETH_ALEN; // length of Mac address
            
            static const MacAddress BROADCAST;

            MacAddress();

            template<typename T>
            MacAddress(T addr[LENGTH]) {
                for (size_t i=0;i<LENGTH;++i)
                    data[i] = addr[i];
            }

            std::string str() const;
            void copy_to(void *target) const;
            
            std::array<uint8_t,LENGTH> data;
    };

    namespace helper {
        bool setBlockingIO(int descriptor, bool blocking);
    }
}

#endif // RANDOM_NET_H_INCLUDED

