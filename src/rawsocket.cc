#include "rawsocket.h"
#include <chrono>
#include <thread>
#include <cassert>
#include <cstring> // memset
#include <unistd.h>
#include <sys/socket.h> // socket
#include <arpa/inet.h> // inet_aton
#include <netdb.h> // getprotobyname
#include <net/if.h> // struct ifreq
#include <sys/ioctl.h>

namespace net {

#if defined(MSG_NOSIGNAL)
    const int RawSocket::IO_FLAGS = MSG_NOSIGNAL;
#else
    const int RawSocket::IO_FLAGS = 0;
#endif

    RawSocket::RawSocket(const std::string& interfaceName) {
        init(interfaceName);
    }

    RawSocket::~RawSocket() {
        close();
    }

    void RawSocket::close() {

        if (!isClosed()) {
            const auto retryDelay = std::chrono::milliseconds(CLOSE_RETRY_DELAY_MSECS);
            
            /* retry if close() fails due to delivery of a signal
             * to prevent descriptor leak
             */
            for (int count=0
                ;::close(descriptor_)==-1 && errno==EINTR && count<MAX_CLOSE_RETRIES
                ;++count) {

                std::this_thread::sleep_for(retryDelay);
            }

            descriptor_ = INVALID_FD;
        }
    }

    void RawSocket::init(const std::string& interfaceName) {
        assert(descriptor_ == INVALID_FD);


        /*
         * create raw socket
         */
        descriptor_ = socket(PF_PACKET,SOCK_RAW,IPPROTO_RAW);

        if (descriptor_ == -1) {
            // TODO: log error
            perror("socket");
            return;
        }

        struct ifreq ifr;
        /*
         * obtain the interface index
         */
        memset(&ifr, 0, sizeof(struct ifreq));
        strncpy(ifr.ifr_name, interfaceName.c_str(), IFNAMSIZ-1);
        if (ioctl(descriptor_, SIOCGIFINDEX, &ifr) < 0) {
            perror("ioctl SIOCGIFINDEX");
            close();
            return;
        }
        iface_idx_ = ifr.ifr_ifindex;

        /*
         * obtain mac address for this interface
         */
        memset(&ifr, 0, sizeof(struct ifreq));
        strncpy(ifr.ifr_name, interfaceName.c_str(), IFNAMSIZ-1);
        if (ioctl(descriptor_, SIOCGIFHWADDR, &ifr) < 0) {
            perror("ioctl SIOCGIFHWADDR");
            close();
            return;
        }
        source_address_ = MacAddress(ifr.ifr_hwaddr.sa_data);
    
#       if defined(SO_NOSIGPIPE)
            // Block SIGPIPE, the BSD/Apple way
            setsockopt(SOL_SOCKET,SO_NOSIGPIPE,1);
#       endif // SO_NOSIGPIPE
    }

    bool RawSocket::setsockopt(int level, int flag, int value) {
        assert(!isClosed());
        const int ivalue = value;
        return -1 != ::setsockopt(descriptor_,level,flag,&ivalue,sizeof(ivalue));
    }

    bool RawSocket::sendto(const std::string& data, const MacAddress& target) {
        assert(!isClosed());
        
        struct sockaddr_ll dest;
        std::memset((void*)&dest,0,sizeof(dest));
        dest.sll_family = AF_PACKET;
        dest.sll_ifindex = iface_idx_;
        std::memcpy(dest.sll_addr,(void*)target.data.data(),target.data.size());
        dest.sll_halen = MacAddress::LENGTH;
        
        std::string buf;
        buf.reserve(data.size() + sizeof(struct ether_header));
        buf.resize(sizeof(struct ether_header));
        struct ether_header* eth = (struct ether_header*)&buf[0];
        target.copy_to(eth->ether_dhost);
        source_address_.copy_to(eth->ether_shost);
        eth->ether_type = htons(ETH_P_IP);

        buf.append(data);
        
        return (ssize_t)buf.size() == ::sendto(descriptor_,
                                       buf.data(),buf.size(),
                                       IO_FLAGS,
                                       (sockaddr*)&dest,sizeof(dest));
    }
    
    bool RawSocket::recvfrom(std::string& data, MacAddress& source) {
        assert(!isClosed());
        // TODO: keep state
        data.resize(recv_buffer_size_);
        struct sockaddr_ll addr;
        std::memset(&addr,0,sizeof(addr));
        socklen_t addr_size = sizeof(addr);
        ssize_t result = ::recvfrom(descriptor_,&data[0],data.size(),IO_FLAGS,
                                    (struct sockaddr*)&addr,&addr_size);
        if (result > 0) {
            data.resize(result);
            source = MacAddress(addr.sll_addr);
            return true;
        }
        return false;
    }

} // namespace net

