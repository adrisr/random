#include "udpsocket.h"
#include "rawsocket.h"
#include <iostream>
#include <thread>

#define WRAP_FAIL(X) do { \
    if ( !(X) ) { \
        std::cerr << "Error: '" << #X << "' failed\n"; \
        return 2; \
    } } while(0)

int client(const char *device) {
    net::RawSocket raw_socket(device);
    const auto target = net::MacAddress::BROADCAST;
    //uint8_t x[6] {8,0,0x27,0x64,0xbb,0xbb};
    //const auto target = net::MacAddress(x);
    std::array<uint8_t,72> data {0x45,0,0,0x48,0x77,0x30,0,0,0x40,0x11,0xe8,0x6f,
        0x0a,3,3,1,0xa,3,3,0xff,0xe1,0x15,0xe1,0x15,0,0x34,0x56,0xf2,
0x53,0x70,0x6f,0x74,0x55,0x64,0x70,0x30,0x09,0xd6,0x58,0x95,0x05,0xcc,0xe5,0x62,
0x00,0x01,0x00,0x04,0x48,0x95,0xc2,0x03,0xf9,0x47,0xa7,0x79,0xba,0xce,0x37,0x98,
0xec,0x0d,0xf6,0xd1,0x4a,0x00,0x7c,0x67,0xf9,0xc8,0xb4,0x77};
    for(;;) {
        if (raw_socket.sendto(std::string((char*)data.data(),data.size()),target)) {
            std::cerr << "sent packet to " << target.str() << ".\n";
            std::this_thread::sleep_for(std::chrono::seconds(3));
        }
        else {
            return 3;
        }
    }
    return 0;
}

void respond(net::RawSocket& sock, const std::string& packet) {
    
}

int server(const char *device) {
    net::UdpSocket socket;
    WRAP_FAIL(socket.setAllowBroadcasts());
    WRAP_FAIL(socket.setReuseAddress());
    net::RawSocket raw_socket(device);

    WRAP_FAIL(socket.bind(net::IPv4Address(net::IPv4Address::ANY_ADDRESS,67)));

    for(;;) {
        std::string packet;
        net::IPv4Address addr;
        if (socket.recvfrom(packet,addr)) {
            std::cerr << "Received packet of size " << packet.size() 
                      << " from " << addr.str() << '\n';
            if (addr.port() == 68
             && packet[0] == 1 // bootrequest
             && packet[1] == 1 // ethernet
             && packet[2] == 6
             && packet.find("\x35\x1\x1")!=packet.npos) {
                uint32_t xid = *((uint32_t*)&packet[4]);
                std::cerr << "Packet is DHCPDISCOVER xid " << xid << '\n';
                respond(raw_socket,packet);
            }
        }
        else {
            return 3;
        }
    }
    return 0;
}

int main(int argc, char *argv[]) {
    
    return argc>2? client(argv[1]) : server(argv[1]);
}

