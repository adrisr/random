#include "net.h"
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cassert>
#include <cstring>
#include <arpa/inet.h> // inet_aton

namespace net {

    const MacAddress MacAddress::BROADCAST
                        ((uint8_t[MacAddress::LENGTH]){255,255,255,255,255,255});

    uint32_t IPv4Address::parse_dotted_quad_address(const std::string& s) {
        struct in_addr addr;
        bool inet_aton_failed = 0==inet_aton(s.c_str(),&addr);
        // TODO: error or warning
        assert(!inet_aton_failed);
        return addr.s_addr;
    }

    void IPv4Address::init(uint32_t ip, uint16_t port) {
        std::memset(&data_,0,sizeof(data_));
        data_.sin_family = AF_INET;
        data_.sin_port = htons(port);
        data_.sin_addr.s_addr = htonl(ip);
    }

    std::string IPv4Address::str() const {
        std::string retval;
        const socklen_t size = INET_ADDRSTRLEN + 10;
        retval.resize(size);
        inet_ntop(AF_INET,&data_.sin_addr.s_addr,&retval[0],size);
        retval.resize(::strlen(retval.c_str()));
        retval.push_back(':');
        retval.append(std::to_string(ntohs(data_.sin_port)));
        return retval;

    }

    MacAddress::MacAddress() {
        data.fill(0);
    }
    
    void MacAddress::copy_to(void *target) const {
        std::memcpy(target,data.data(),data.size());
    }

    std::string MacAddress::str() const {
        std::stringstream ss;
        ss << std::hex << std::setfill('0') << std::setw(2);
        bool first = true;
        for (auto c : data) {
            if (!first) ss << ':';
            else first = false;
            ss << (unsigned int)c;
        }
        return ss.str();
    }

    bool helper::setBlockingIO(int descriptor, bool blocking) {
        assert(descriptor>=0);
        const int flags = fcntl(descriptor,F_GETFL,0);
        const bool isBlocking = !(flags & O_NONBLOCK);

        if (blocking != isBlocking) {
            return -1 != fcntl(descriptor,F_SETFL,flags ^ O_NONBLOCK);
        }
        return true;
    }
}

