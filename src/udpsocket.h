#ifndef RANDOM_UDP_SOCKET_H_INCLUDED
#define RANDOM_UDP_SOCKET_H_INCLUDED

#include "net.h"

namespace net {

    class UdpSocket {
        public:
            UdpSocket();
            ~UdpSocket();
            
            void close();

            bool isClosed() const { 
                return descriptor_ == -1;
            }

            bool setAllowBroadcasts(bool flag = true);
            bool setReuseAddress(bool flag = true);

            bool setBlocking(bool wantBlocking = true) {
                return helper::setBlockingIO(descriptor_,wantBlocking);
            }

            bool setsockopt(int level, int flag, int value);
            
            bool attachToDevice(const std::string& device);

            bool bind(const IPv4Address& address);
            
            bool sendto(const std::string& data, const IPv4Address& dest);
            bool recvfrom(std::string& data, IPv4Address& src);

            void setRecvBufferSize(size_t size) {
                recv_buffer_size_ = size;
            }

        private:
            void init();

            static constexpr int MAX_CLOSE_RETRIES = 10;
            static constexpr int CLOSE_RETRY_DELAY_MSECS = 100;
            static constexpr int INVALID_FD = -1;
            static constexpr size_t DEFAULT_RECV_BUFFER_SIZE = 1024;
            static const int IO_FLAGS; // for sendto() and recvfrom() MSG_NOSIGNAL

            int descriptor_ = INVALID_FD;
            size_t recv_buffer_size_ = DEFAULT_RECV_BUFFER_SIZE;
    };
}

#endif //RANDOM_UDP_SOCKET_H_INCLUDED
